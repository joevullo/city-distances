/*
 * FILE: menu.h
 *
 * TITLE: Menu Header File.
 *
 * DATE: 05.04.2013
 *
 * PURPOSE: This file is the header file containing the class definitions for the menu class.
 *
 * FUNCTIONS:
 *
 * 	public:
 * 		Menu(CityTree *) - Constructor for the class.
 * 		void displayMenu();
 * 	private:
 * 		CityTree *tree;
 * 		bool changesMade;
 * 		void listAll();
 * 		void addCity();
 * 		void editCity();
 * 		void removeCity();
 * 		void calculate();
 * 		void saveAll();
 * 		void quit();
 * 		void printTree();
 *
 * INCLUDED FILES: "CityTree.h"
 *
 */

#ifndef _MENU_
#define _MENU_

#include "citytree.h"

namespace WorldDistances
{
	class Menu
	{
		public:
			Menu(CityTree *);
			void displayMenu();
		private:
			CityTree *tree;
			bool changesMade;
			void listAll();
			void addCity();
			void editCity();
			void removeCity();
			void calculate();
			void saveAll();
			void quit();
			void printTree();
	};
}
#endif

