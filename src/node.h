/*
 * FILE: node.h
 *
 * TITLE: Node header file.
 *
 * DATE: 24/02/2013
 *
 * PURPOSE:
 * This file is a header file providing the functionality for the node class, which shall be used to store
 * cities in a binary tree.
 *
 *
 * FUNCTIONS:
 *
 * INCLUDED FILES:
 * 	'city.h' - This is the header file for the city class.
 *
 *
 */

#ifndef _NODE_
#define _NODE_

#include "city.h"

namespace WorldDistances
{
	class city;

	class Node
	{
		private:
        	City *city;
        	Node *parent;
        	Node *left;
        	Node *right;
		public:
        	Node(City *);
        	Node *getLeft() { return left; }
        	Node *getRight() { return right; }
        	Node *getParent() { return parent; }
        	City *getCity() { return city; }
        	void setCity(City *newCity) { city = newCity; }
        	void setParent(Node *newParent) { parent = newParent; }
        	void setLeft(Node *newLeft) { left = newLeft; }
        	void setRight(Node *newRight) { right = newRight; }
	};
}
#endif
