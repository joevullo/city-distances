/*
 * FILE:
 *
 * TITLE:
 *
 * DATE:
 *
 * PURPOSE:
 *
 * OVERALL METHOD:
 *
 * FUNCTIONS:
 *
 * INCLUDED FILES:
 *
 *
 */

#include<iostream>

#include "node.h"
#include "city.h"

using namespace std;

namespace WorldDistances
{
	/* Parameter City *theCity: This is the city objects to be added to the node.
	 *
	 * Desc: The constructor for a node, which sets up its links till initially be null
	 * and adds a pointer to the city object passed in.
	 */
	Node::Node(City * theCity )
	{
		city = new City();
		city = theCity;
		left = NULL;
		right = NULL;
		parent = NULL;
	};


}






