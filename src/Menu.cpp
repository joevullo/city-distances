/*
 * FILE: Menu.cpp
 *
 * TITLE: Menu Class Implementation.
 *
 * DATE: 24/02/2013
 *
 * PURPOSE:
 * This file provides the implementation for the menu class, which is used to provide the user interface to the
 * user of the program.
 *
 * FUNCTIONS:
 *
 * INCLUDED FILES:
 * 	iostream
 * 	string
 * 	string.h
 * 	stdlib.h
 * 	"menu.h"
 *
 */


#include<iostream>
#include<string>
#include<string.h>
#include <stdlib.h>

#include "menu.h"

using namespace std;

namespace WorldDistances
{
	Menu::Menu(CityTree *cityTree)
	{
		changesMade = false;
		tree = cityTree;
	}
	/*
	 *
	 * Desc: Displays the menu to the user, allowing them to choose from
	 * a range of options.
	 */
	void Menu::displayMenu()
	{
		cin.clear();
		int choice = 0;

		cout << "--Distance Between Cities Program--" << endl;
		cout << "1 = List all cities" << endl;
		cout << "2 = Calculate Distance between cities" << endl;
		cout << "3 = Add city" << endl;
		cout << "4 = Edit city" << endl;
		cout << "5 = Remove city" << endl;
		cout << "6 = Save all changes" << endl;
		cout << "0 = Quit program" << endl;
		cout << "Please enter an option from above: ";

		do
		{
			cin >> choice;
			switch(choice)
			{
				case 1:
					listAll(); 		// list cities
					displayMenu();
					break;
				case 2:
					calculate();	//calculate distances
					displayMenu();
					break;
				case 3:
					addCity();		//add city
					displayMenu();
					break;
				case 4:
					editCity();		//edit city
					displayMenu();
					break;
				case 5:
					removeCity();	//remove city
					displayMenu();
					break;
				case 6:
					saveAll();		//save city
					displayMenu();
					break;
				case 99:
					printTree(); //debugging for tree
					cout << "\n";
					displayMenu();
					break;
				case 0:
					quit();			//quite menu
					break;
				default:
					cout << "Incorrect input" << endl << endl;
					displayMenu();
					break;
			}
		} while(choice != 0);
	}

	/*
	 * Desc: Calls the tree method display all which lists all the cities from within the tree
	 */
	void Menu::listAll()
	{
		tree->displayAll();
	}

	/*
	 * Desc:  Starts the dialog to add a city to the tree, asking the user
	 * to enter all the attributes of the city they want to enter, then
	 * creates a city object using these attributes and passes it as a parameter
	 * calling the tree's add city method.
	 */
	void Menu::addCity()
	{
		string name, country, latHemisphere, longHemisphere;
		int longitude[2], latitude[2];
		double longitudeDecimal, latitudeDecimal;
		changesMade = true; //This is set to true to show that changes are made to the original tree.

		cout << "Adding new city.\n";
		cout << "Please enter the name: "; cin >> name;
		cout << "Please enter the country: "; cin >> country;
		cout << "Please enter the latitude (degrees): "; cin >> latitude[0];
		cout << "Please enter the latitude (minutes): "; cin >> latitude[1];
		cout << "Please enter the latitude (hemisphere): "; cin >> latHemisphere;
		cout << "Please enter the longitude (degrees): "; cin >> longitude[0];
		cout << "Please enter the longitude (minutes): "; cin >> longitude[1];
		cout << "Please enter the longitude (hemisphere): "; cin >> longHemisphere;

		longitudeDecimal = longitude[0] + (double)longitude[1]/60;
		latitudeDecimal = latitude[0] + (double)latitude[1]/60;

		City *city = new City();
		city->setName(name);
		city->setCountry(country);
		city->setHemisphere(latHemisphere + longHemisphere);
		city->setLongitude(longitudeDecimal);
		city->setLatitude(latitudeDecimal);
    	city->setID();

    	tree->addCity(city);
	}
	/*
	 * Desc: Asks the user to enter the name & country of city they wish to edit, then finds
	 * that city and calls edit city using the city found as a parameter.
	 */
	void Menu::editCity()
	{
		changesMade = true;
		string name, country;

		cout << "Please enter the name of the city you wish to edit: " << endl;
		cin >> name;
		cout << "Please enter the country of the city you wish to edit: " << endl;
		cin >> country;
		City *city = tree->findCity(name + country);
		
		if(city == NULL)
		{
		    cout << "City not found." << endl;
		    city = tree->findCityByIncompleteName(name);
            if(city != NULL)
            {
                cout << "Did you mean " << city->getName() << "?" << endl;
                tree->editCity(city);
            }
        }
		else
		{
    		tree->editCity(city);
        }
	}

	/*
	 * Desc: Asks the user to enter the city name & country that they wish to remove
	 * and then calls the remove city function of the tree passing the city the user
	 * wishes to remove.
	 */
	void Menu::removeCity()
	{
		changesMade = true;

		string name, country;
		cout << "Please enter the name of the city: " << endl;
		cin >> name;
		cout << "Please enter the country of the city: " << endl;
		cin >> country;

		City *city = tree->findCity(name + country);
		
		if(city == NULL)
        {
   		    cout << "City not found." << endl;
        }
		else
    		tree->removeCity(city);
	}

	/*
	 *  Desc: Asks the user to enter to cities and calls the tree's
	 *  findDistance on the chosen cities.
	 */
	void Menu::calculate()
	{
		//Ask for citys/countrys then display list of results (ID/Name/Country)
		//then user selects ID from list. If so we only need to ask for the
		//country name.
		string fromName, fromCountry, toName, toCountry;

		cout << "Please enter the first city name: ";
		cin >> fromName;
		cout << "Please enter the first country name: ";
		cin >> fromCountry;

		cout << "Please enter the second city name: ";
		cin >> toName;
		cout << "Please enter the second country name: ";
		cin >> toCountry;

		//Retrive nodes from IDs (using search method?)
		City *from = new City();
		City *to = new City();

		from = tree->findCity(fromName + fromCountry);
		to = tree->findCity(toName + toCountry);
		//tree.findDistance needs both city nodes passed through.
		
		if(from == NULL)
		{
            cout << "Oops, you misspelt the first city's name and/or country." << endl;
            from = tree->findCityByIncompleteName(fromName);
            if(from != NULL)
            {
                cout << "Did you mean " << from->getName() << "?" << endl;
            }
        }
        if(to == NULL)
		{
            cout << "Oops, you misspelt the second city's name and/or country." << endl;
            to = tree->findCityByIncompleteName(toName);
            if(to != NULL)
            {
                cout << "Did you mean " << to->getName() << "?" << endl;
            }
        }
        
        if(from != NULL && to != NULL)
        {
            cout << endl;
    		cout << "Distance between " << from->getName() << ", " << from->getCountry();
    		cout << " and " << to->getName() << ", " << to->getCountry() << " is; " << endl;
            cout << tree->findDistance(from, to) << "km or " << (tree->findDistance(from, to)*0.6214) << "miles" << endl;
            cout << endl;
    		cout << "Walking time: " << (tree->findDistance(from, to)/5.632704) * 60 << " minutes! (avg 5.63km an hour)" << endl;
    		cout << "Running time: " << (tree->findDistance(from, to)/8.04672) * 60 << " minutes! (avg 8.04km an hour)" << endl;
			cout << "Driving time: " << (tree->findDistance(from, to)/96.56) * 60 << " minutes! (avg 96.56km an hour)" << endl;
    		cout << "In-air flight time: " <<  (tree->findDistance(from, to)/965.6064) * 60 << " minutes! (avg 965.60km an hour)" << endl;
        }
	}

	/*
	 * Desc: Saves the changes made to the tree to a file.
	 */
	void Menu::saveAll()
	{
		tree->initiateSaveToFile();
		changesMade = false;
	}


	/*
	 * Desc: Quits the program, asking the user if they wish to save their changes
	 * if any changes have been made.
	 */
	void Menu::quit()
	{
		char shouldISave;
		if(changesMade)
		{
			cout << "We have noticed that you have made some changes to the data. Would you like to save it? (Y/N) " << endl;
			cin >> shouldISave;
			if(shouldISave == 'Y' || shouldISave == 'y')
				saveAll();
		}
		cout << "Program will now exit \n";

		exit(EXIT_SUCCESS);
	}

	/*
	 * This will call the display graphic tree function of the tree, which
	 * will display the current tree level by level.
	 */
	void Menu::printTree()
    {
		int beginPosition = 0, paddingRemover = 0;

		tree->printGraphicTree(tree->getRoot(), beginPosition, paddingRemover);
	}

}
