/*
 * FILE: city.h
 *
 * TITLE: City Header File.
 *
 * DATE: 24/03/13
 *
 * PURPOSE:
 * This is the header file containing the functionality for the city class, which is used to store information
 * about the cities stored within the program.
 *
 * FUNCTIONS:
 * 	
 * INCLUDED FILES:
 *
 * 	<strings.h>.
 * 	"node.h" - This contains the functionality for the node class.
 */

#ifndef _CITY_
#define _CITY_

#include<string>

#include "node.h"

using namespace std;

namespace WorldDistances
	{
		class Node;
		class City
		{
			private:
				Node *parentNode;
				string name;
				string country;
				string id;
				string hemisphere;
				double longitude;
				double latitude;
			public:
                City(City *);
                City();
				Node *getParentNode() { return parentNode; }
				string getName() { return name; }
				string getCountry() { return country; }
				string getID() { return id; }
				string getHemisphere() { return hemisphere; }
				double getLongitude() { return longitude; }
				double getLatitude() { return latitude; }
				void setParentNode(Node *newParentNode) { parentNode = newParentNode; }
				void setName(string newName) { name = newName; }
				void setCountry(string newCountry) { country = newCountry; }
				void setHemisphere(string newHemisphere) { hemisphere = newHemisphere; }
				void setLongitude(double newLongitude) { longitude = newLongitude; }
				void setLatitude(double newLatitude) { latitude = newLatitude; }
				void setID() { id = name + country; }
			};
	}
#endif
