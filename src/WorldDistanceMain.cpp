/*
 * FILE: WorldDistanceMain.cpp
 *
 * TITLE: World Distance Main.
 *
 * PROJECT TO BE SUBMITTED BY:
 * Botond Butuza - 11073330
 * Rob Carpenter - 11064198
 * Malcolm Eller - 11065097
 * Joseph Vullo - 11027969
 *
 * DATE: 24/02/2013
 *
 * PURPOSE:
 *
 *
 * FUNCTIONS:
 *  int main() - Starts the program building a city tree, passing that into this object
 * into the new menu object into its constructor. 
 *
 * INCLUDED FILES:
 *  #include<iostream>
 *  #include "node.h"
 *  #include "city.h"
 *  #include "citytree.h"
 *  #include "menu.h"
 *
 *
 */

#include<iostream>

#include "node.h"
#include "city.h"
#include "citytree.h"
#include "menu.h"

using namespace std;
using namespace WorldDistances;

int main()
{
	WorldDistances::CityTree *tree;
	tree = new CityTree();
	WorldDistances::Menu menu(tree);
	menu.displayMenu();

	return 0;
}


