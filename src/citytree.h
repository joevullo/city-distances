/*
 * FILE: citytree.h
 *
 * TITLE: City Tree Header.
 *
 * DATE: 24/02/2013
 *
 * PURPOSE:
 * This file is the header file which contains the class definition for
 * the city tree.
 *
 * FUNCTIONS:
 *
 * INCLUDED FILES:
 * 		node.h
 *
 */

#include<sstream>
#include<fstream>

#ifndef _CITYTREE_
#define _CITYTREE_

#include "node.h"

namespace WorldDistances
{
	class CityTree
	{
		private:
			Node *root;
			ofstream filePointer;

			Node *addNode(Node *, Node *);
			City *search(Node *, string);
			City *searchByIncompleteName(Node *, string);
			void print(Node *);
			void saveToFile(Node *);
			int height(Node *);
			Node *getLastUnbalancedNode(Node *);
			void rotateRight(Node *);
			void rotateLeft(Node *);
			void rotateDoubleRight(Node *);
			void rotateDoubleLeft(Node *);
			void balanceALLTheNode(Node *);
		public:
			CityTree();
			Node *getRoot() { return root; }
			City *findCity(string);
			City *findCityByIncompleteName(string);
			void addCity(City *);
			void editCity(City *);
			void removeCity(City *);
			bool doesCityAlreadyExist(string);
			int readFromFile();
			double findDistance(City *, City *);
			void printGraphicTree(Node *, int, int);
			void initiateSaveToFile();
			void displayAll();
        	void balance(Node *);
	};
}
#endif

