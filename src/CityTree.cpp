/*
 * FILE: CityTree.cpp
 *
 * TITLE: City Tree Implementation.
 *
 * DATE: 04/04/2013
 *
 * PURPOSE: This file providers the main implementations for the city tree.
 * It handles all the functionalities of the binary tree that we
 * use to implement our program.
 *
 * FUNCTIONS:
 *
 * INCLUDED FILES:
 * 		iostream
 * 		string
 * 		sstream
 * 		fstream
 * 		cmath
 * 		stdlib.h
 * 		conio.h
 * 		citytree.h
 * 		node.h
 */

#include<iostream>
#include<string>
#include<cmath>
#include<stdlib.h>
//#include<conio.h>

#include <cstdlib>
#include <queue>

#include "citytree.h"
#include "node.h"
#include <limits.h>

namespace WorldDistances
{
	/* Desc: The constructor for the city tree which declares it is called
	 * initialises root and reads the cities in from the file.
	 */
	CityTree::CityTree()
	{
		//cout << "	CityTree constructor called." << endl;
		root = NULL;
		readFromFile();
	}

	/* Pre: That a file exists that contains a list of cities in the correct format.
	 * Post: City Tree contains cities read in from the file.
	 *
	 * Desc: Reads the cities from a file, creates the relevant
	 * classes and adds them to the existing tree.
	 *
	 */
	int CityTree::readFromFile()
	{
        cout << "    Reading from file." << endl;
		string line;
        //set of strings to store input per line

        string inputCity;
        string inputCountry;
        string inputLatDeg;
        string inputLatMin;
        string inputLatComp;
        string inputLongDeg;
        string inputLongMin;
        string inputLongComp;
        ifstream theInputFile("cities.txt");

        if(theInputFile.is_open())
        {
            while(theInputFile.good())//While there are more lines in the file
            {
                getline(theInputFile, line); // get the next line and put it into line string
                std::istringstream ins(line);// Initialise input stream with line

                // load words
                std::string out;

                for(int i = 0; i < 8; i++)//per word on line
                {
                    getline(ins, out, ','); // getline stops at ','
                    switch (i)
                    {   // Reads in each part of each line into the program
                        case 0: inputCity = out; break;
                        case 1: inputCountry = out; break;
                        case 2: inputLatDeg = out; break;
                        case 3: inputLatMin = out; break;
                        case 4: inputLatComp = out; break;
                        case 5: inputLongDeg = out; break;
                        case 6: inputLongMin = out; break;
                        case 7: inputLongComp = out; break;
                    }
                }

                City *city = new City();
                city->setName(inputCity);
                city->setCountry(inputCountry);
                city->setHemisphere(inputLatComp + inputLongComp);
                city->setLongitude(atof(inputLongDeg.c_str()) + atof(inputLongMin.c_str())/60);
                city->setLatitude(atof(inputLatDeg.c_str()) + atof(inputLatMin.c_str())/60);
                city->setID();

                addCity(city);
            }
            theInputFile.close();
        }
        else
            cout << "Unable to open file";

        return 0;
	}

	/* Pre: That the parameters is a well formed city object.
	 * Parameter: City *city - which is a city object to be added to the tree.
	 *
	 * Desc: Adds a city (the parameter) to the existing city tree,
	 * if said tree does not already exist, then balances the tree
	 */
	void CityTree::addCity(City *city)
	{
		bool isDupe = doesCityAlreadyExist(city->getID());
		//cout << "    is dupe? " << isDupe << endl;

		if(!isDupe)
		{
            Node *node = new Node(city);
            root = addNode(root, node);
            city->setParentNode(node);
            root->setParent(root);
            
            balanceALLTheNode(root);
		}
		else
		{
			cout << "This is a duplicate city." << endl;
		}
	}

	/* Pre both the city and branch parameters are well formed.
	 * Para Node *branch
	 * Para Node *city
	 *
	 * Desc adds a city to a node and adds that node to the tree.
	 */
	Node *CityTree::addNode(Node *branch, Node *city)
	{
		if(branch == NULL)
		{
			//cout << "	Added to root." << endl;
			branch = city;
			branch->setParent(branch); //if root, parent = itself.
		}
		else if(city->getCity()->getID().compare(branch->getCity()->getID()) < 0)
		{
			branch->setLeft(addNode(branch->getLeft(), city));
			//cout << "	Added to left subtree at " << branch->getCity()->getName() << endl;
			branch->getLeft()->setParent(branch);
		}
		else
		{
			branch->setRight(addNode(branch->getRight(), city));
			//cout << "	Added to right subtree at " << branch->getCity()->getName() << endl;
			branch->getRight()->setParent(branch);
		}

		return branch;
	}

	/*
	 * Para string id: The name of city to test.
	 *
	 * Desc: Tests weather the string passed in already exists as a
	 * city name, if the name exists return false, otherwise true.
	 *
	 */
	bool CityTree::doesCityAlreadyExist(string id)
	{
		return search(root, id);
	}


	City *CityTree::findCity(string id)
	{
		return search(root, id);
	}

	City *CityTree::findCityByIncompleteName(string name)
	{
        City *pointer = new City();

        while(name.size() > 1)
        {
            pointer = searchByIncompleteName(root, name);
            if(pointer != NULL)
                return pointer;

            name = name.substr(0, name.size()-1);
        }

        return NULL;
    }

	City *CityTree::search(Node *branch, string id)
	{
		if(branch == NULL)
		{
			return NULL;
		}
		else
		{
			if(id.compare(branch->getCity()->getID()) == 0)
				return branch->getCity();
			else if(id.compare(branch->getCity()->getID()) < 0)
				return search(branch->getLeft(), id);
			else
				return search(branch->getRight(), id);
		}
	}

	City *CityTree::searchByIncompleteName(Node *branch, string name)
	{
		if(branch == NULL)
		{
			return NULL;
		}
		else
		{
            string cityName = branch->getCity()->getName();
			if(cityName.find(name) != string::npos)
				return branch->getCity();
			else if(name.compare(cityName) < 0)
				return searchByIncompleteName(branch->getLeft(), name);
			else
				return searchByIncompleteName(branch->getRight(), name);
		}
	}

	/*
	 * Parameter City *cityToEdit:
	 *
	 * Desc: Gives the user a choice of all the city attributes they wish to edit.
	 * allowing them to edit any attribute of the city. If the user edits the city name
	 * or country then before the changes are made they are checked to see if any duplicates can
	 * exist. Edit city will remove that node from the tree and add it again if changes are
     * made to the city name or country. This is done to ensure that the node will be in the correct place
     * place in tree.
	 */
	void CityTree::editCity(City *cityToEdit)
	{
   		bool doesExist;
		int choice = 0;
		int cont;

		if (root == NULL)
		{
             cout << "There are no cities! - Enter any key to go back" << endl;
             cin >> cont;
		}

		cout << "Which attribute of " << cityToEdit->getName() << " would you like to edit." << endl;
		cout << "1 = City Name" << endl;
		cout << "2 = Cities Country" << endl;
		cout << "3 = City Hemisphere" << endl;
		cout << "4 = City Longitude" << endl;
		cout << "5 = City Latitude" << endl;
		cout << "6 = Quit back to menu" << endl;
		cin >> choice;

		switch(choice)
		{
			case 1:
            {
                //Change city name
				string newName;
				cout << "Current name is " << cityToEdit->getName() << "\n Please enter a new city name." << endl;
				cin >> newName;
				doesExist = doesCityAlreadyExist(newName + cityToEdit->getCountry());
				if(doesExist)
				{
					cout << "Do you want to: " << endl;
					cout << "1 = Try again? " << endl;
					cout << "2 = Quit to menu " <<endl;
					cin >> choice;
					switch (choice)
					{
						case 1:
							editCity(cityToEdit);
							break;
						case 2:
							return;
					}
				}
				else
				{
					cityToEdit->setName(newName);
					cityToEdit->setID();
					//Delete the edited city, if the ID has changed and re-add it, to ensure it is in the correct place in the tree.
					City *newCity = new City(cityToEdit);
					removeCity(cityToEdit);
					addCity(newCity);
				}
				break;
            }
			case 2:
            {
				//Change country name.
				string newCountry;
				cout << "Current country is " << cityToEdit->getCountry() <<"\n Please enter a new city country." << endl;
				cin >> newCountry;
				doesExist = doesCityAlreadyExist(cityToEdit->getName() + newCountry);
				if(doesExist)
				{
					cout << "Do you want to: " << endl;
					cout << "1 = Try again? " << endl;
					cout << "2 = Quit to menu " <<endl;
					cin >> choice;
					switch (choice)
					{
						case 1:
							editCity(cityToEdit);
							break;
						case 2:
							return;
					}
				}
				else
				{
					cityToEdit->setCountry(newCountry);
					cityToEdit->setID();
					//Delete the edited city, if the ID has changed and re-add it, to ensure it is in the correct place in the tree.
					City *newCity = new City(cityToEdit);
					removeCity(cityToEdit);
					addCity(newCity);
				}
				break;
            }
			case 3:
            {
				//Change hemisphere.
				string newHemisphere;
				cout << "Current Hemisphere is " << cityToEdit->getHemisphere() << "\n Please enter a new city Hemisphere." << endl;
				cin >> newHemisphere;
				cityToEdit->setHemisphere(newHemisphere);
				break;
            }
			case 4:
            {
				//Change longitude.
				double newLongitude;
				cout << "Current longitude is " << cityToEdit->getLongitude() << "\n Please enter a new city longitude." << endl;
				cin >> newLongitude;
				cityToEdit->setLongitude(newLongitude);
				break;
            }
			case 5:
            {
				//Change latitude.
				double newLatitude;
				cout << "Current latitude is " << cityToEdit->getLatitude() << "\n Please enter a new city latitude." << endl;
				cin >> newLatitude;
				cityToEdit->setLatitude(newLatitude);
				break;
            }
			case 6:
				return;
			default:
				//Error, or incorrect value entered.
				cout << "Sorry incorrect value entered, please try again." << endl;
				editCity(cityToEdit);
				break;
   	    }
	}

	/* Parameter CityToRemove: The city in questions to remove.
	 *
	 * This removes city which is the parameter, re attaching any nodes
	 * that are now lose and then calling balance to correct any issues.
	 */
	void CityTree::removeCity(City *cityToRemove)
	{
		cout << "Removing City\n";

		Node *nodeToRemove = cityToRemove->getParentNode();

		//Root with no children.
		if (nodeToRemove->getLeft() == NULL && nodeToRemove->getRight() == NULL && nodeToRemove->getParent() == nodeToRemove)
        {
			//cout << "This is a root with no children.\n";
			root = NULL;
			delete nodeToRemove->getCity();
			delete nodeToRemove;
			balanceALLTheNode(root);
			return;
		}

		//Root, with just left child.
		if (nodeToRemove->getParent() == nodeToRemove && nodeToRemove->getLeft() != NULL && nodeToRemove->getRight() == NULL)
        {
			//cout << "This is a root with just left child.\n";
			root = nodeToRemove->getLeft();
			nodeToRemove->getLeft()->setParent(root);
			
			delete nodeToRemove->getCity();
			delete nodeToRemove;
			balanceALLTheNode(root);
			return;
		}

		//Root, with just right child.
		if (nodeToRemove->getParent() == nodeToRemove && nodeToRemove->getRight() != NULL && nodeToRemove->getLeft() == NULL)
        {
			//cout << "This is a root with just right child.\n";
			root = nodeToRemove->getRight();
			nodeToRemove->getRight()->setParent(root);
			delete nodeToRemove->getCity();
			delete nodeToRemove;
			balanceALLTheNode(root);
			return;
		}


		//Root node with both a left and a right child.
		if (nodeToRemove->getParent() == nodeToRemove && nodeToRemove->getRight() != NULL && nodeToRemove->getLeft() != NULL)
        {
			//cout << "This is a root with left & right child.\n";
			
			int leftHeight  = height(nodeToRemove->getLeft());
			int rightHeight = height(nodeToRemove->getRight());
			
			if(leftHeight >= rightHeight)
			{
                Node *furthestRightNode = nodeToRemove->getLeft()->getRight();
			    Node *previousNode = nodeToRemove->getLeft();
			    
                while(furthestRightNode != NULL)
                {
    				previousNode = furthestRightNode;
    				furthestRightNode = furthestRightNode->getRight();
    			}
    			
    			furthestRightNode = previousNode;

                nodeToRemove->getCity()->setName(furthestRightNode->getCity()->getName());
                nodeToRemove->getCity()->setCountry(furthestRightNode->getCity()->getCountry());
                nodeToRemove->getCity()->setID();
                nodeToRemove->getCity()->setLongitude(furthestRightNode->getCity()->getLongitude());
                nodeToRemove->getCity()->setLatitude(furthestRightNode->getCity()->getLatitude());
                nodeToRemove->getCity()->setHemisphere(furthestRightNode->getCity()->getHemisphere());

    			removeCity(furthestRightNode->getCity());
            }
            else
            {
                Node *furthestLeftNode = nodeToRemove->getRight()->getLeft();
			    Node *previousNode = nodeToRemove->getRight();
			    
                while(furthestLeftNode != NULL)
                {
    				previousNode = furthestLeftNode;
    				furthestLeftNode = furthestLeftNode->getLeft();
    			}
    			
    			furthestLeftNode = previousNode;

                nodeToRemove->getCity()->setName(furthestLeftNode->getCity()->getName());
                nodeToRemove->getCity()->setCountry(furthestLeftNode->getCity()->getCountry());
                nodeToRemove->getCity()->setID();
                nodeToRemove->getCity()->setLongitude(furthestLeftNode->getCity()->getLongitude());
                nodeToRemove->getCity()->setLatitude(furthestLeftNode->getCity()->getLatitude());
                nodeToRemove->getCity()->setHemisphere(furthestLeftNode->getCity()->getHemisphere());

    			removeCity(furthestLeftNode->getCity());
            }
            
			return;
		}

		//Node with parent and no children.
		if (nodeToRemove->getParent() != nodeToRemove &&  nodeToRemove->getLeft() == NULL && nodeToRemove->getRight() == NULL)
        {
			//cout << "This is a node with parent and no children.\n";
			//Find which link of the parent is joined and set to null.
			if (nodeToRemove->getParent()->getLeft() == nodeToRemove)
            {
				nodeToRemove->getParent()->setLeft(NULL);
			}
            else
            {
				nodeToRemove->getParent()->setRight(NULL);
			}
			
			delete nodeToRemove->getCity();
			delete nodeToRemove;
			balanceALLTheNode(root);
			return;
		}

		//If node with parent with left and no right.
		if (nodeToRemove->getParent() != nodeToRemove &&  nodeToRemove->getLeft() != NULL && nodeToRemove->getRight() == NULL)
        {
			//cout << "This is a node with parent and a left child.\n";
			//Find which link of the parent that joins it to the nodeToRemove.

			if (nodeToRemove->getParent()->getLeft() == nodeToRemove)
            {
				nodeToRemove->getParent()->setLeft(nodeToRemove->getLeft());
			} 
            else
            {
				nodeToRemove->getParent()->setRight(nodeToRemove->getLeft());
			}
			nodeToRemove->getLeft()->setParent(nodeToRemove->getParent());
			
			delete nodeToRemove->getCity();
			delete nodeToRemove;
			balanceALLTheNode(root);
			return;
		}

		//If node with parent with right and no left.
		if (nodeToRemove->getParent() != nodeToRemove &&  nodeToRemove->getLeft() == NULL && nodeToRemove->getRight() != NULL)
        {
			//cout << "This is a node with parent and a right child.\n";
			//Find which link of the parent that joins it to the nodeToRemove.

			if (nodeToRemove->getParent()->getLeft() == nodeToRemove)
            {
				nodeToRemove->getParent()->setLeft(nodeToRemove->getRight());
			}
            else
            {
				nodeToRemove->getParent()->setRight(nodeToRemove->getRight());
			}
			nodeToRemove->getRight()->setParent(nodeToRemove->getParent());
			
			delete nodeToRemove->getCity();
			delete nodeToRemove;
			balanceALLTheNode(root);
			return;
		}

		//If node with parent with left and right.
		if (nodeToRemove->getParent() != nodeToRemove &&  nodeToRemove->getLeft() != NULL && nodeToRemove->getRight() != NULL)
        {
			//cout << "This is a node with parent and a left and right child.\n";
			
			int leftHeight  = height(nodeToRemove->getLeft());
			int rightHeight = height(nodeToRemove->getRight());
			
			if(leftHeight >= rightHeight)
			{
                Node *furthestRightNode = nodeToRemove->getLeft()->getRight();
			    Node *previousNode = nodeToRemove->getLeft();
			    
                while(furthestRightNode != NULL)
                {
    				previousNode = furthestRightNode;
    				furthestRightNode = furthestRightNode->getRight();
    			}
    			
    			furthestRightNode = previousNode;

                nodeToRemove->getCity()->setName(furthestRightNode->getCity()->getName());
                nodeToRemove->getCity()->setCountry(furthestRightNode->getCity()->getCountry());
                nodeToRemove->getCity()->setID();
                nodeToRemove->getCity()->setLongitude(furthestRightNode->getCity()->getLongitude());
                nodeToRemove->getCity()->setLatitude(furthestRightNode->getCity()->getLatitude());
                nodeToRemove->getCity()->setHemisphere(furthestRightNode->getCity()->getHemisphere());

    			removeCity(furthestRightNode->getCity());
            }
            else
            {
                Node *furthestLeftNode = nodeToRemove->getRight()->getLeft();
			    Node *previousNode = nodeToRemove->getRight();
			    
                while(furthestLeftNode != NULL)
                {
    				previousNode = furthestLeftNode;
    				furthestLeftNode = furthestLeftNode->getLeft();
    			}
    			
    			furthestLeftNode = previousNode;

                nodeToRemove->getCity()->setName(furthestLeftNode->getCity()->getName());
                nodeToRemove->getCity()->setCountry(furthestLeftNode->getCity()->getCountry());
                nodeToRemove->getCity()->setID();
                nodeToRemove->getCity()->setLongitude(furthestLeftNode->getCity()->getLongitude());
                nodeToRemove->getCity()->setLatitude(furthestLeftNode->getCity()->getLatitude());
                nodeToRemove->getCity()->setHemisphere(furthestLeftNode->getCity()->getHemisphere());

    			removeCity(furthestLeftNode->getCity());
            }
            
            balanceALLTheNode(nodeToRemove);
			return;
		}

	}

	/*
	 * Displays a complete list of all the cities within the tree structure,
	 * by using another method called print. This method is so that the list
	 * displayed will be in a readable format.
	 */
	void CityTree::displayAll()
	{
        cout.precision(3);
		if (root == NULL)
		{
             cout << "There are no cities! - Press any key to go back";
		}
		else
		{
			//Header
			cout << endl << "List of all cities..." << endl;
			cout << "City Name" << "\t\t";
			cout << "Country" << "\t\t";
			cout << "Hemisphere" << "\t";
			cout << "Long" << "\t";
			cout << "Lat" << endl << endl;

			print(root);
		}
		cout << endl;

	}
	/* Param Node *branch - The branch to print, which will used be called recursively.
     *
	 * Desc: Used to print all the of the cities within the tree, this is a recursive method.
	 */
	void CityTree::print(Node *branch)
	{
		if (branch != NULL)
		{
			print(branch->getLeft());
			cout << branch->getCity()->getName() << "\t\t";
			if(branch->getCity()->getName().length() <= 7)
            { // fix for tabing on strins less than 7 in length
                cout << "\t";
			}
			cout << branch->getCity()->getCountry() << "\t\t";
			if(branch->getCity()->getCountry().length() <= 7)
            {
                cout << "\t";
			}
			cout << branch->getCity()->getHemisphere() << "\t";
			cout << branch->getCity()->getLongitude() << "\t";
			cout << branch->getCity()->getLatitude() << endl;
			print(branch->getRight());
		}
	}

	/* Param City *city1 - The first city to find the distance.
     * Param City *city2 - The second city to find the distance between the first.
     *
	 * Finds the distance between two city objects using a calculation of their
	 * latitude and latitude to find their distances.
	 */
	double CityTree::findDistance(City *City1, City *City2)
	{
		double pi = atan(1)*4;
		
		double fromLatitude = City1->getLatitude();
		double toLatitude = City2->getLatitude();
		
		double fromLongitude = City1->getLongitude();		
		double toLongitude = City2->getLongitude();

		string fromHemisphere = City1->getHemisphere();
		if(fromHemisphere.substr(0,1).compare("N") == 0)
		    fromLatitude *= (-1);
		if(fromHemisphere.substr(1,2).compare("E") == 0)
		    fromLongitude *= (-1);
		
		string toHemisphere = City2->getHemisphere();
		if(toHemisphere.substr(0,1).compare("N") == 0)
		    toLatitude *= (-1);
		if(toHemisphere.substr(1,2).compare("E") == 0)
		    toLongitude *= (-1);

		double latitudeArc  = (fromLatitude - toLatitude) * pi/180;
		double longitudeArc = (fromLongitude - toLongitude) * pi/180;
		
		double latitudeH = sin(latitudeArc * 0.5);
		double lontitudeH = sin(longitudeArc * 0.5);
		
		latitudeH *= latitudeH;
		lontitudeH *= lontitudeH;
		
		double tmp = cos(City1->getLatitude()*pi/180) * cos(City2->getLatitude()*pi/180);
		double forAsin = sqrt(latitudeH + tmp*lontitudeH);
		double asinValue = asin(forAsin);
		
		double returnValue = 6371.0 * 2.0 * asinValue;
		
		return returnValue;
	}

	/*
	 * Initiates the save to file, checking along the way cities are currently
	 * stored within the tree and opening the output file to save the cities in.
	 */
    void CityTree::initiateSaveToFile()
    {
        if (root == NULL)
		{
             cout << "There are no cities! - Press any key to go back";
		}
		else
        {
            //clear file
            filePointer.open ("cities.txt", ios::trunc);
            filePointer.close();

            filePointer.open("cities.txt", ios::app);
            saveToFile(root);
            filePointer.close();
		}
    }

	/*
     *
	 * Desc: Saves the current binary tree information, i.e its current cities to a file when
	 * this method is called.
	 */
	void CityTree::saveToFile(Node *branch)
	{
		// left node
	    if (branch->getLeft() != NULL)
		{
		    saveToFile(branch->getLeft());
		}

		int latDegrees, longDegrees, latMinutes, longMinutes;

		// Convert decimal to degrees - add .5 to minutes to fix rounding error
		// Typecasting the minutes to int to avoid getting a warning of loss of precision
        latDegrees = (int)branch->getCity()->getLatitude();
        latMinutes = (int)((branch->getCity()->getLatitude() - latDegrees) * 60 + 0.5);

		longDegrees = (int)branch->getCity()->getLongitude();
        longMinutes = (int)((branch->getCity()->getLongitude() - longDegrees) * 60 + 0.5);

        filePointer << branch->getCity()->getName() << "," << branch->getCity()->getCountry() << "," << latDegrees << "," << latMinutes << "," << branch->getCity()->getHemisphere()[0] << "," << longDegrees << "," << longMinutes << "," <<  branch->getCity()->getHemisphere()[1] <<"\n";

        // right node
        if (branch->getRight() != NULL)
		{
		    saveToFile(branch->getRight());
		}
    }

	/*
	 * A recursive method used to gather the height of the tree's nodes.
	 */
    int CityTree::height(Node *branch)
    {
        if(branch == NULL)
            return 0;

        int left  = height(branch->getLeft());
        int right = height(branch->getRight());

        return (left > right)? left+1 : right+1;
    }

    /*
     * A recursive method used to get the trees "last unbalanced node" or the furthest
     * node down the tree which is unbalanced so that the balancing method can be called
     * from the bottom of the tree up, ensuring that a triangle like tree is formed.
     */
    Node *CityTree::getLastUnbalancedNode(Node *branch)
    {
        if(branch == NULL)
            return NULL;

        int leftHeight  = height(branch->getLeft());
        int rightHeight = height(branch->getRight());

        // this is going to be the first balanced node, so return its parent,
        // which is then the first unbalanced node
        if(abs(leftHeight - rightHeight) < 2)
        {
            return branch->getParent();
        }

        // left heavy
        if(leftHeight >= rightHeight)
        {
            return getLastUnbalancedNode(branch->getLeft());
        }

        // right heavy
        return getLastUnbalancedNode(branch->getRight());
    }

    /**
     * Post-order traversal because we need to balance from the bottom upwards
     */
    void CityTree::balanceALLTheNode(Node *branch)
    {
        if(branch->getLeft() != NULL)
            balanceALLTheNode(branch->getLeft());
        
        if(branch->getRight() != NULL)
            balanceALLTheNode(branch->getRight());
            
        balance(branch);
    }


    /*
     * The main functions to balance the tree, which does this based on several criteria
     * and then calls the relevant method to balance the tree.
     */
	void CityTree::balance(Node *branch)
	{
        int left  = height(branch->getLeft());
        int right = height(branch->getRight());

        // do we need to balance?
        if(abs(left - right) < 2)
            return;

   		// RIGHT RIGHT
   		if(right - left > 2)
   		{
            rotateLeft(branch);
            //rotateDoubleLeft(branch);
        }
        // SINGLE RIGHT
        else if(right - left > 1)
        {
            rotateLeft(branch);
        }
        // LEFT LEFT
        else if(left - right > 2)
        {
            rotateRight(branch);
            //rotateDoubleRight(branch);
        }
        // SINGLE LEFT
        else
        {
            rotateRight(branch);
        }
	}

	/*
	 * Performs a single rotation left on the node in question.
	 */
	void CityTree::rotateLeft(Node *branch)
	{
        //cout << "       Balancing to the left at " << branch->getCity()->getName() << endl;
        Node *newParent = branch->getRight(); //Get the right of the last unbalanced node.

        branch->setRight(newParent->getLeft()); //Set the right of the last unbalanced node to its child's right.

        if (branch->getRight() != NULL)
        	branch->getRight()->setParent(branch); //Set parent of newParents left.
        	
        newParent->setLeft(branch);

        // set pointers in the nodes connected above the last unbalanced
        if(branch == root)
        {
            root = newParent;
            newParent->setParent(root);
        }
        else if (branch->getParent()->getRight() == branch)
        {
            branch->getParent()->setRight(newParent);
        }
        else
        {
            branch->getParent()->setLeft(newParent);
        }

        //Set parent nodes.
        newParent->setParent(branch->getParent());
        branch->setParent(newParent);
    }

	/*
	 * Performs a single rotation right on the node in question.
	 */
    void CityTree::rotateRight(Node *branch)
    {
        //cout << "       Balancing to the right at " << branch->getCity()->getName() << endl;
        Node *newParent = branch->getLeft();

        branch->setLeft(newParent->getRight());
        if (branch->getLeft() != NULL)
        	branch->getLeft()->setParent(branch); //Settings np right parent.
        	
        newParent->setRight(branch);

        // root node
        if(branch == root)
        {
            root = newParent;
            newParent->setParent(root);
        }
        else if(branch->getParent()->getRight() == branch)
        {
            branch->getParent()->setRight(newParent);
        }
        else
        {
            branch->getParent()->setLeft(newParent);
        }

        //Set parent nodes.
        newParent->setParent(branch->getParent());
        branch->setParent(newParent);

    }

    void CityTree::rotateDoubleRight(Node *branch)
    {
        rotateLeft(branch->getRight());
        rotateRight(branch);
    }

    void CityTree::rotateDoubleLeft(Node *branch)
    {
        rotateRight(branch->getLeft());
        rotateLeft(branch);
    }

	/*
	 * Prints the contents of the tree using breadth first traversal to print all the
	 * items off in level order, and then uses queues to store the information
	 * so that it can be printed out in order.
	 *
	 * This method is only used to debug the tree and ensure that the tree is balancing
	 * the values correctly.
	 */
    void CityTree::printGraphicTree(Node * branch, int position, int paddingRemover)
    {
    	//todo - make this work a bit better, could use \b to go back on each line and set up
    	// A better tree like look, based on if parent->getLeft == current or vice versa.
    	if (!root)
    		return;

    	queue<Node*> currentRow, nextRow;
    	//int i;
    	currentRow.push(root); //push the root node on the current queue.

    	/*Sets up the padding for each line.
    	for (i = 0; i < position; i++) {
    		cout << " ";
    	}*/
        int i = 1;
    	while (!currentRow.empty()) {
    		Node *currentNode = currentRow.front(); //Current node equals he front
    	    currentRow.pop(); //Call the front and remove it.
    	    if (currentNode) {
    	    	if (currentNode->getParent() != NULL) {
    	    		string parent = currentNode->getParent()->getCity()->getName();

    	    		cout /*<< i << ". " */<< currentNode->getCity()->getName() /* << "(" << parent <<")"*/ << endl;// << height(currentNode->getLeft()) << " " << height(currentNode->getRight()) << ")" << " "; //Print the city name.
    	    		i++;

    	    		//TESTING WORK
        	    	/*if (currentNode->getLeft() != NULL){
        	    		string childLeft = currentNode->getLeft()->getCity()->getName();
        	    		cout << " (l = " << childLeft << ")";
        	    	}
        	    	if (currentNode->getRight() != NULL){
        	    		string childRight = currentNode->getRight()->getCity()->getName();
        	    		cout << "(r = " << childRight << ")";
        	    	}*/ //END OF TESTING



    	    	} else {
    	    		cout << currentNode->getCity()->getName() << " "; //Print the city name.
    	    	}
    	    	//Push left and right onto the current node.
    	    	if(currentNode->getLeft() != NULL) {
    	    		nextRow.push(currentNode->getLeft());
    	    	}

    	    	if(currentNode->getRight() != NULL) {
    	    		nextRow.push(currentNode->getRight());
    	    	}
    	    }
    	    if (currentRow.empty()) {
    	    	cout << endl << endl;

    	    	position-= paddingRemover;
    	    	swap(currentRow, nextRow);
    	    }
    	  }

    }
}

