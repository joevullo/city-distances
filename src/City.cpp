/*
 * FILE: City.cpp
 *
 * TITLE: City Implementation.
 *
 * DATE: 04/04/2013
 *
 * PURPOSE: This file implements a constructor for city that makes 
 * a copy of the city passed to the constructor 
 *
 * FUNCTIONS:
 * City(City *) - This isn't a function but a contructor which makes a 
 * copy of another city.
 *
 * INCLUDED FILES:
 * 		iostream
 * 		string
 * 		stdlib.h
 * 		citytree.h
 * 		node.h
 */

#include<iostream>
#include<string>
#include<stdlib.h>

#include "citytree.h"
#include "node.h"

namespace WorldDistances
{
    City::City(City *cityToCopy)
	{
        setParentNode(cityToCopy->getParentNode()); 
		setName(cityToCopy->getName());
		setCountry(cityToCopy->getCountry());
		setID();
		setHemisphere(cityToCopy->getHemisphere());
		setLongitude(cityToCopy->getLongitude());
		setLatitude(cityToCopy->getLatitude());
    }
    
    City::City()
    {
        
    }
}
