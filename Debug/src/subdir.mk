################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/City.cpp \
../src/CityTree.cpp \
../src/Menu.cpp \
../src/Node.cpp \
../src/WorldDistanceMain.cpp 

OBJS += \
./src/City.o \
./src/CityTree.o \
./src/Menu.o \
./src/Node.o \
./src/WorldDistanceMain.o 

CPP_DEPS += \
./src/City.d \
./src/CityTree.d \
./src/Menu.d \
./src/Node.d \
./src/WorldDistanceMain.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


